package com.blo.model;

import java.util.List;

import lombok.Data;

@Data
public class Users {
	private List<User>users;

	public Users(List<User> userList) {
		super();
		this.users = userList;
	}
	
	

}
