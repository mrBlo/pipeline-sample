package com.blo.model;

import lombok.Data;

@Data
public class User {
	private String firstName;
	private String email;
	private String lastName;
	private Gender gender;
	public User(String firstName, String email, String lastName, Gender gender) {
		super();
		this.firstName = firstName;
		this.email = email;
		this.lastName = lastName;
		this.gender = gender;
	}
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	

}
