package com.blo.model;

public enum Gender {
	MALE,
	FEMALE,
	UNKNOWN
}
