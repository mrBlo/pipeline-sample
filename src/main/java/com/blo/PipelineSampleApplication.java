package com.blo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PipelineSampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(PipelineSampleApplication.class, args);
	}

}
