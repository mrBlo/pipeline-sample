package com.blo.controller;


import java.util.Arrays;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.blo.model.Gender;
import com.blo.model.User;
import com.blo.model.Users;


@RestController
//@RequestMapping(value = "/users")
public class UserController {


	
	@GetMapping(value = "/")
	public String welcome(){
		return "Welcome. Use \"/users\" to view users endpoint";
	}
	
	
	@GetMapping(value = "/users")
	public Users getAll(){
		return	new Users(Arrays.asList(
					new User("Yaw","yaw@yahoo.com","Mensah",Gender.MALE),
					new User("Akosua","akosuaEm@gmail.com","Bruce",Gender.FEMALE)));  
	}
	
	
	
	
}
